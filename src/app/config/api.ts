import { environment } from "src/environments/environment";

export const baseUrl = environment.production ? 'http://localhost:4200/' : 'http://localhost:8080'
export const productsUrl = baseUrl + '/product'
export const cartUrl = baseUrl + '/cart'
export const userUrl = baseUrl + '/user'