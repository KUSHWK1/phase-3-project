import { Component } from '@angular/core';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css']
})
export class AddproductComponent {

  private product: Product;

  constructor(private service: ProductService, private router: Router) {
    this.product = new Product();
   }

  addProduct(){
    this.service.addProduct(this.product).subscribe(data=>
      {
        this.product = data;
        this.router.navigate(['/shop']);
      })
  }
}
