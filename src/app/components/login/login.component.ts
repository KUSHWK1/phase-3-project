import { Component, OnInit, Output } from '@angular/core';
import { RegisterService } from 'src/app/services/register.service';
import { Router } from '@angular/router';
import { Register } from 'src/app/models/register';
import { MessengerService } from 'src/app/services/messenger.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: any = {}
 @Output() user: Register;
  username: string;

  constructor(private service: RegisterService, private router: Router, private msgservice: MessengerService) { }

  ngOnInit() {
  }

  login(){
    console.log(this.model);
    this.service.login(this.model.username).subscribe(value=>{
      this.user = value;
      if(this.model.username === value.username && this.model.password === value.password){
        this.msgservice.sendUser(value);
        this.router.navigate(['/shop']);
      }else{
        this.router.navigate(['/login']);
      }
    })
  }
}
