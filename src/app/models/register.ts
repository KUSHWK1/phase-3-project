export class Register {
    userid: number;
    name: string;
    username: string;
    password: string;
    email: string;
}
