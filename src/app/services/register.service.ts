import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { userUrl } from 'src/app/config/api'
import { Register } from '../models/register';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) { }

  public addUser(register: Register){
    return this.http.post<Register>(userUrl,register);
  }

  public login(username: string): Observable<Register>{
    return this.http.get<Register>(userUrl+"/"+username);
  }
}
