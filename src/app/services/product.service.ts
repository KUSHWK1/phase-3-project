import { Injectable } from '@angular/core';
import { Product } from '../models/product';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { productsUrl } from 'src/app/config/api'

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  
  constructor(private http: HttpClient) { }

  public getAllProducts():Observable<Product[]>{
    return this.http.get<Product[]>(productsUrl+"s");
  }

  public addProduct(product: Product){
    return this.http.post<Product>(productsUrl,product);
  }

  public getProductById(id: number): Observable<Product>{
    return this.http.get<Product>(productsUrl+"/"+id);
  }
}
